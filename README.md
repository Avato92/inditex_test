# Podcast App

En este proyecto, he desarrollado una aplicación que permite al usuario buscar y explorar una lista de podcasts, cada uno con detalles específicos sobre los capítulos de ese podcast.

## Tecnologías

- Vite: he utilizado Vite como herramienta de construcción y desarrollo de la aplicación. Vite es un constructor web ultrarrápido que permite la recarga rápida en tiempo real y genera aplicaciones web altamente optimizadas y eficientes. Con Vite, puedo construir aplicaciones web rápidamente, sin tener que preocuparme por la configuración manual de herramientas como Webpack o Parcel.

- React: he utilizado React como biblioteca de componentes para la interfaz de usuario de la aplicación. React es una biblioteca popular para la creación de interfaces de usuario en JavaScript. Es una herramienta poderosa para el desarrollo de aplicaciones web de una sola página y permite una estructura modular y reutilizable de la interfaz de usuario.

- TypeScript: he utilizado TypeScript como lenguaje de programación para la aplicación. TypeScript es un superconjunto de JavaScript que agrega tipos estáticos y otras características para mejorar la legibilidad, mantenibilidad y escalabilidad del código. Con TypeScript, puedo escribir código más seguro y evitar errores comunes de JavaScript.

- React Router: he utilizado React Router como biblioteca de enrutamiento para la aplicación. React Router es una biblioteca popular para la creación de enrutadores en aplicaciones web de React. Permite la navegación entre diferentes vistas en la aplicación de una manera intuitiva y fácil de usar.

- Redux Toolkit: he utilizado Redux Toolkit como herramienta para gestionar el estado global de la aplicación. Redux Toolkit es una biblioteca que incluye utilidades y conveniencias para simplificar el uso de Redux, incluyendo la creación de store, reducers y acciones. Con Redux Toolkit, puedo gestionar de manera más eficiente el estado global de la aplicación, manteniendo un flujo de datos unidireccional y predecible en mi código.

- ESLint: he utilizado ESLint como linter para la aplicación. ESLint es una herramienta que ayuda a encontrar y corregir errores en el código. Con ESLint, puedo asegurarme de que mi código cumpla con las reglas de estilo definidas y evitar errores comunes en el código.

## Instrucciones de instalación y ejecución

1. Clona el repositorio en tu máquina local:

```bash
git clone git@gitlab.com:Avato92/inditex_test.git
```

2. Accede al directorio del proyecto:

```bash
cd inditex_test
```

3. Instala dependencias del proyecto

```bash
npm i
```

3. Inicia la aplicación en modo desarrollo:

```bash
npm run start:dev
```

## Comandos

### `npm run start:dev`

Este comando se utiliza para iniciar la aplicación en modo de desarrollo. Con este comando, puedo ver mi aplicación en el navegador mientras trabajo en ella. Cualquier cambio que haga en el código se reflejará en el navegador de inmediato gracias a la recarga rápida en tiempo real (hot-reloading).

### `npm run start:prod`

Este comando se utiliza para iniciar la aplicación en modo de producción. La aplicación se construirá y se servirá en un servidor web local en mi computadora. Este comando es útil para verificar que mi aplicación funcione correctamente antes de implementarla en producción.

### `npm run build`

Este comando se utiliza para construir la aplicación para producción. El resultado es una versión optimizada y reducida de mi aplicación que se puede implementar en un servidor web. Este comando utiliza TypeScript para compilar mi código y Vite para generar una versión optimizada de mi aplicación.

### `npm run lint`

Este comando se utiliza para ejecutar el linter en mi código y asegurarme de que mi código cumpla con las reglas de estilo definidas en mi configuración de ESLint. Este comando también puede ayudarme a encontrar errores en mi código y mejorar la calidad de mi aplicación.

## Versionado

Para mantener un registro de los diferentes estados importantes de la aplicación, se utiliza el sistema de tags de Git. Los tags se utilizan para marcar puntos específicos en la historia del repositorio, como versiones estables o lanzamientos importantes.

Para crear un nuevo tag en el repositorio, se puede utilizar el siguiente comando:

```bash
git tag <nombre-del-tag>
```

Donde <nombre-del-tag> es el nombre que se desea asignar al tag. Por ejemplo, si se desea crear un tag para la versión 1.0 de la aplicación, se puede utilizar el siguiente comando:

```bash
git tag v1.0.0
```

Para listar todos los tags existentes en el repositorio, se puede utilizar el siguiente comando:

```bash
git tag
```

Para enviar los tags al repositorio remoto, es necesario utilizar el siguiente comando:

```bash
git push --tags
```

Este comando envía todos los tags que existen en el repositorio local al repositorio remoto, permitiendo que otros miembros del equipo puedan acceder a ellos y utilizarlos según sea necesario.

Es importante tener en cuenta que los tags se utilizan para marcar puntos específicos en la historia del repositorio y no para realizar cambios en el código fuente de la aplicación. Por lo tanto, se recomienda utilizarlos únicamente para indicar versiones estables o lanzamientos importantes.

Este proyecto seguirá el modo de versionado [Semantic Versioning 2.0.0](https://semver.org/), que establece el formato de las versiones y las reglas para incrementarlas.

El número de versión se compone de tres números separados por un punto: MAJOR.MINOR.PATCH. Cada uno de estos números tiene un significado específico:

- **MAJOR:** Incrementar este número cuando se realizan cambios que no son compatibles con versiones anteriores.

- **MINOR:** Incrementar este número cuando se añaden nuevas funcionalidades de forma retrocompatible.

- **PATCH:** Incrementar este número cuando se realizan correcciones de errores de forma retrocompatible

Además, también se podrán añadir etiquetas a las versiones, como alpha, beta o rc, para indicar que se trata de una versión en desarrollo o de pruebas. Por ejemplo, 1.0.0-alpha.1 indica la primera versión alpha de la futura versión 1.0.0.

Para el control de versiones se utilizará Git, y se creará un tag para cada nueva versión que se publique.

## Arquitectura

Para la realización de la prueba se ha elegido usar una arquitectura hexagonal.

La arquitectura hexagonal, también conocida como arquitectura de puertos y adaptadores, es un enfoque de diseño de software que se centra en la separación de las diferentes capas y componentes de una aplicación. El objetivo de esta arquitectura es hacer que el código sea más modular, escalable y mantenible.

En la arquitectura hexagonal, se divide la aplicación en tres capas principales: la capa de dominio, la capa de aplicación y la capa de infraestructura. Cada capa tiene una responsabilidad específica y está diseñada para ser independiente de las demás capas.

La capa de dominio es donde se encuentra la lógica de negocio de la aplicación. Aquí se definen las entidades, los objetos de valor y las reglas de negocio que rigen la aplicación.

La capa de aplicación es la capa intermedia que se encarga de orquestar la interacción entre la capa de dominio y la capa de infraestructura. Esta capa contiene los casos de uso y los servicios que se utilizan para llevar a cabo las acciones que se definen en la capa de dominio.

La capa de infraestructura es donde se encuentran las implementaciones concretas de los componentes técnicos que utiliza la aplicación, como bases de datos, servicios web, servicios de correo electrónico, etc.

La arquitectura hexagonal tiene varias ventajas, entre las que se incluyen:

- Modularidad: la separación en capas hace que la aplicación sea más fácil de entender y modificar, lo que facilita el mantenimiento y la evolución de la aplicación.

- Escalabilidad: la separación en capas hace que la aplicación sea más fácil de escalar en función de las necesidades, ya que se pueden cambiar los componentes de la capa de infraestructura sin afectar a la lógica de negocio de la aplicación.

- Testabilidad: la separación en capas hace que la aplicación sea más fácil de probar, ya que se pueden aislar las diferentes capas y probarlas de forma independiente.

En resumen, la arquitectura hexagonal es una forma efectiva de construir aplicaciones que son escalables, mantenibles y fáciles de entender y modificar.

## Arbol de directorios

El arbol de directorios de la aplicación es el siguiente:

```
src/
|-- core/
|   |-- domain/
|   |-- use-cases/
|   |-- interfaces/
|-- infrastructure/
|   |-- components/
|   |-- services/
|   |-- repositories/
|-- presentation/
|   |-- components/
|   |-- pages/
|   |-- layouts/
|   |-- routers/
```

Estos directorios contendrán la siguiente información, haciendo relación con el punto anterior donde se explica la arquitectura hexagonal.

- core/: Contiene la lógica de negocio de la aplicación, que se divide en tres subcarpetas:

  - domain/: Define los modelos de dominio y las reglas de negocio de la aplicación.
  - usecases/: Implementa los casos de uso que utilizan los modelos de dominio para realizar operaciones específicas de la aplicación.
  - ports/: Define los contratos que deben implementar los adaptadores para comunicarse con el núcleo de la aplicación.

- infrastructure/: Contiene los adaptadores que interactúan con el mundo exterior y que implementan los contratos definidos en la carpeta core/interfaces/. Se divide en tres subcarpetas:

  - components/: Define los componentes de React que se utilizan para mostrar la interfaz de usuario.
  - services/: Define los servicios que se utilizan para comunicarse con servicios externos (por ejemplo, una API REST).
  - repositories/: Define los repositorios que se utilizan para obtener y almacenar datos.

- presentation/: Contiene los adaptadores que interactúan con el usuario y que implementan los contratos definidos en la carpeta core/interfaces/. Se divide en cuatro subcarpetas:

  - components/: Define los componentes de React que se utilizan para mostrar la interfaz de usuario.
  - pages/: Define las páginas de la aplicación que se muestran al usuario.
  - layouts/: Define los diseños de página que se utilizan para envolver los componentes.
  - routers/: Define las rutas de navegación de la aplicación.

## Mejoras

- Arquitectura hexagonal
- Se ha añadido una función de debounce para el filtro de podcast (deprecado) Después de leer detenidamente las exigencias de las pruebas he decidio quitar la función de debounce
