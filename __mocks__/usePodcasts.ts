// __mocks__/usePodcasts.ts

import { PodcastEntry } from "../src/core/domain/podcast";

export const mockPodcasts: PodcastEntry[] = [
  {
    "im:name": { label: "Podcast 1" },
    "im:artist": { label: "Author 1", attributes: { href: "test" } },
    "im:image": [
      { label: "https://example.com/image.jpg", attributes: { height: "100" } },
    ],
    "im:releaseDate": {
      label: "2022-04-24T00:00:00.000Z",
      attributes: { label: "uno" },
    },
    id: { label: "1", attributes: { "im:id": "1" } },
    link: {
      attributes: {
        href: "https://example.com/podcast1",
        type: "example",
        rel: "example",
      },
    },
    category: {
      attributes: {
        label: "Category 1",
        scheme: "schema",
        term: "term",
        "im:id": "im:id",
      },
    },
    summary: { label: "Summary 1" },
    "im:price": {
      label: "gratis",
      attributes: { amount: "nada", currency: "euros" },
    },
    "im:contentType": { attributes: { term: "term", label: "label" } },
    rights: { label: "nada" },
    title: { label: "Podcast 1" },
  },

  {
    "im:name": { label: "Podcast 2" },
    "im:artist": { label: "Author 2", attributes: { href: "test" } },
    "im:image": [
      { label: "https://example.com/image.jpg", attributes: { height: "100" } },
    ],
    "im:releaseDate": {
      label: "2022-04-24T00:00:00.000Z",
      attributes: { label: "uno" },
    },
    id: { label: "2", attributes: { "im:id": "2" } },
    link: {
      attributes: {
        href: "https://example.com/podcast1",
        type: "example",
        rel: "example",
      },
    },
    category: {
      attributes: {
        label: "Category 2",
        scheme: "schema",
        term: "term",
        "im:id": "im:id",
      },
    },
    summary: { label: "Summary 2" },
    "im:price": {
      label: "gratis",
      attributes: { amount: "nada", currency: "euros" },
    },
    "im:contentType": { attributes: { term: "term", label: "label" } },
    rights: { label: "nada" },
    title: { label: "Podcast 2" },
  },
];

export default jest.fn().mockReturnValue({
  podcasts: mockPodcasts,
  loading: false,
});
