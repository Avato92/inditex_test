import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { EpisodeEntry, PodcastDetailsInfo } from "../../core/domain/episode";
import { RestEpisodeRepository } from "../../infraestructure/repositories/episode-repository";
import { ListEpisodesUseCase } from "../../core/usecases/list-episodes-usecase";

const VITE_DETAILS_PODCAST_URL = import.meta.env.VITE_DETAILS_PODCAST_URL;

if (!VITE_DETAILS_PODCAST_URL) {
  throw new Error("VITE_DETAILS_PODCAST_URL is not defined");
}

const restEpisodeRepository = new RestEpisodeRepository(
  VITE_DETAILS_PODCAST_URL
);

const useEpisodes = (podcastId: string) => {
  const [episodes, setEpisodes] = useState<Array<EpisodeEntry>>([]);
  const [podcastInfo, setPodcastInfo] = useState<PodcastDetailsInfo>();
  const [loading, setLoading] = useState(true);
  const navigate = useNavigate();

  if (!podcastId || podcastId === "") navigate("/");

  useEffect(() => {
    const listEpisodes = new ListEpisodesUseCase(restEpisodeRepository);
    listEpisodes.execute(podcastId).then((episodes) => {
      const { podcastInfo, episodeEntry } = episodes;
      setEpisodes(episodeEntry);
      setPodcastInfo(podcastInfo);
      setLoading(false);
    });
  }, [podcastId]);

  return { episodes, podcastInfo, loading };
};

export default useEpisodes;
