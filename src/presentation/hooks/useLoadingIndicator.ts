import { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";

const useLoadingIndicator = () => {
  const [isLoading, setIsLoading] = useState(false);
  const location = useLocation();

  useEffect(() => {
    setIsLoading(true);

    const handleLoad = () => {
      setIsLoading(false);
    };

    window.addEventListener("load", handleLoad);

    return () => {
      window.removeEventListener("load", handleLoad);
    };
  }, [location.pathname]);

  return isLoading;
};

export default useLoadingIndicator;
