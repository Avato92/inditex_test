import { useState, useMemo } from "react";
import { PodcastEntry } from "../../core/domain/podcast";

const usePodcastFilter = (podcasts: PodcastEntry[], initialValue: string) => {
  const [filterValue, setFilterValue] = useState<string>(initialValue);
  const [filteredPodcasts, setFilteredPodcasts] = useState<PodcastEntry[]>([]);

  const handleChange = (ev: React.ChangeEvent<HTMLInputElement>) => {
    const value = ev.target.value.toLowerCase();
    setFilterValue(value);
  };

  useMemo(() => {
    const filtered = podcasts.filter((podcast) => {
      const name = podcast["im:name"].label.toLowerCase();
      const author = podcast["im:artist"].label.toLowerCase();

      return name.includes(filterValue) || author.includes(filterValue);
    });
    setFilteredPodcasts(filtered);
  }, [filterValue, podcasts]);

  return { filteredPodcasts, filterValue, handleChange };
};

export default usePodcastFilter;
