import { ListPodcastsUseCase } from "./../../core/usecases/list-podcasts-usecase";
import { RestPodcastRepository } from "./../../infraestructure/repositories/podcast-repository";
import { useState, useEffect } from "react";
import { PodcastEntry } from "../../core/domain/podcast";

const VITE_PODCASTS_URL = import.meta.env.VITE_PODCASTS_URL;

if (!VITE_PODCASTS_URL) {
  throw new Error("VITE_PODCASTS_URL is not defined");
}

const restPodcastRepository = new RestPodcastRepository(VITE_PODCASTS_URL);

const usePodcasts = () => {
  const [podcasts, setPodcasts] = useState<PodcastEntry[]>([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const listPodcasts = new ListPodcastsUseCase(restPodcastRepository);

    listPodcasts.execute().then((podcasts) => {
      setPodcasts(podcasts);
      setLoading(false);
    });
  }, []);

  return { podcasts, loading };
};

export default usePodcasts;
