import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import PodcastList from "../pages/PodcastList/PodcastList";
import EpisodeList from "../pages/EpisodeList/EpisodeList";
import EpisodeDetails from "../pages/EpisodeDetails/EpidoseDetails";

function AppRouter() {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<PodcastList />} />
        <Route path="/podcast/:podcastId" element={<EpisodeList />} />
        <Route
          path="/podcast/:podcastId/episode/:episodeId"
          element={<EpisodeDetails />}
        />
      </Routes>
    </Router>
  );
}

export default AppRouter;
