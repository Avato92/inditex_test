import Badge from "../../components/Badge/Badge";
import Header from "../../layouts/Header/Header";
import usePodcasts from "../../hooks/usePodcasts";
import { PodcastEntry } from "../../../core/domain/podcast";
import PodcastCard from "../../components/PodcastCard/PodcastCard";
import "./podcastList.css";
import usePodcastFilter from "../../hooks/usePodcastFilter";

const PodcastList = () => {
  const { podcasts, loading } = usePodcasts();
  const { filteredPodcasts, filterValue, handleChange } = usePodcastFilter(
    podcasts,
    ""
  );
  return (
    <>
      <Header />
      <main className="podcasts">
        <div className="podcasts-filterSection">
          <Badge value={filteredPodcasts.length || podcasts.length} />
          <input
            type="text"
            value={filterValue}
            onChange={handleChange}
            placeholder="Filter podcasts..."
          />
        </div>
        <section className="podcasts-container">
          {loading && <span>Loading...</span>}
          {filteredPodcasts.length > 0 ? (
            filteredPodcasts.map((podcast: PodcastEntry) => (
              <PodcastCard
                key={podcast.id.attributes["im:id"]}
                podcast={podcast}
              />
            ))
          ) : filterValue ? (
            <span>No se encontraron resultados.</span>
          ) : (
            podcasts.map((podcast: PodcastEntry) => (
              <PodcastCard
                key={podcast.id.attributes["im:id"]}
                podcast={podcast}
              />
            ))
          )}
        </section>
      </main>
    </>
  );
};
export default PodcastList;
