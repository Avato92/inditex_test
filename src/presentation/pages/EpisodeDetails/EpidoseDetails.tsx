import { useSelector } from "react-redux";
import { RootState } from "../../../infraestructure/store/store";
import Header from "../../layouts/Header/Header";
import PodcastsInfo from "../../components/PodcastInfo/PodcastsInfo";
import useEpisodes from "../../hooks/useEpisodes";
import "./episodeDetails.css";

const EpidoseDetails = () => {
  const episode = useSelector(
    (state: RootState) => state.episode.episodeSelected
  );
  const podcastId = useSelector(
    (state: RootState) =>
      state?.podcast?.podcastSelected?.id.attributes["im:id"]
  );
  const summary = useSelector(
    (state: RootState) => state.podcast.podcastSelected?.summary.label
  );

  const { podcastInfo } = useEpisodes(podcastId ? podcastId : "");

  console.log(episode);
  return (
    <>
      <Header />
      <main className="episode-list">
        {podcastInfo && summary && episode && (
          <>
            <PodcastsInfo podcastInfo={podcastInfo} description={summary} />
            <section className="episode-details-player-container">
              <h2>{episode.trackName}</h2>
              <span dangerouslySetInnerHTML={{ __html: episode.description }} />
              <audio className="episode-details-player" controls>
                <source src={episode.episodeUrl} type="audio/mpeg" />
                Tu navegador no soporta el elemento audio.
              </audio>
            </section>
          </>
        )}
      </main>
    </>
  );
};
export default EpidoseDetails;
