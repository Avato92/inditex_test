import { useParams } from "react-router-dom";
import Header from "../../layouts/Header/Header";
import useEpisodes from "../../hooks/useEpisodes";
import "./episodeList.css";
import PodcastsInfo from "../../components/PodcastInfo/PodcastsInfo";
import Table from "../../components/Table/Table";
import { useSelector } from "react-redux";
import { RootState } from "../../../infraestructure/store/store";

const EpisodeList = () => {
  const { podcastId } = useParams();
  const summary = useSelector(
    (state: RootState) => state.podcast.podcastSelected?.summary.label
  );

  const { episodes, podcastInfo, loading } = useEpisodes(
    podcastId ? podcastId : ""
  );

  return (
    <>
      <Header />
      {loading ? (
        <h1>Loading...</h1>
      ) : episodes && podcastInfo ? (
        <main className="episode-list">
          <PodcastsInfo podcastInfo={podcastInfo} description={summary} />
          <section className="episode-list-container">
            <h2 className="episode-list-title">
              Episodes: {podcastInfo?.trackCount}
            </h2>
            {episodes && <Table episodes={episodes} podcastId={podcastId} />}
          </section>
        </main>
      ) : (
        <span>Algo ocurrió</span>
      )}
    </>
  );
};
export default EpisodeList;
