import { useNavigate } from "react-router-dom";
import { PodcastCardProps } from "./types";
import "./podcastCard.css";
import { useDispatch } from "react-redux";
import { selectPodcast } from "../../../infraestructure/store/podcastSlice";

const PodcastCard = ({ podcast }: PodcastCardProps) => {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const handleClick = () => {
    dispatch(selectPodcast(podcast));
    navigate(`/podcast/${podcast.id.attributes["im:id"]}`);
  };

  return (
    <div
      className="podcast-card"
      data-testid="podcast-card"
      onClick={handleClick}
    >
      <div className="podcast-card-image-container">
        <img
          className="podcast-card-image"
          src={podcast["im:image"][0].label}
          alt={podcast["im:name"].label}
        />
      </div>
      <div className="podcast-card-info">
        <h2 className="podcast-card-title">{podcast["im:name"].label}</h2>
        <p className="podcast-card-author">{`Author: ${podcast["im:artist"].label}`}</p>
      </div>
    </div>
  );
};

export default PodcastCard;
