import { render, screen } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";
import PodcastCard from "./PodcastCard";
import { mockPodcasts } from "../../../../__mocks__/usePodcasts";
import "@testing-library/jest-dom";
import { Provider } from "react-redux";
import { store } from "../../../infraestructure/store/store";

describe("PodcastCard component", () => {
  it("should render the podcast image and name", () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <PodcastCard podcast={mockPodcasts[0]} />
        </MemoryRouter>
      </Provider>
    );
    const podcastTitle = screen.getByText("Podcast 1");
    const podcastImage = screen.getByAltText("Podcast 1");
    expect(podcastTitle).toBeInTheDocument();
    expect(podcastImage).toBeInTheDocument();
    expect(podcastImage).toHaveAttribute(
      "src",
      "https://example.com/image.jpg"
    );
  });

  it("should render the podcast author", () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <PodcastCard podcast={mockPodcasts[1]} />
        </MemoryRouter>
      </Provider>
    );
    const podcastAuthor = screen.getByText("Author: Author 2");
    expect(podcastAuthor).toBeInTheDocument();
  });
});
