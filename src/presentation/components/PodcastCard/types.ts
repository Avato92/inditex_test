import { PodcastEntry } from "../../../core/domain/podcast";

export type PodcastCardProps = {
  podcast: PodcastEntry;
};
