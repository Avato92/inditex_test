import { useNavigate } from "react-router-dom";
import { EpisodeEntry } from "../../../core/domain/episode";

import { ITableProps } from "./types";
import TableRow from "../TableRow/TableRow";

const Table = ({ episodes, podcastId }: ITableProps) => {
  const navigate = useNavigate();

  if (!podcastId) navigate("/");

  return (
    <table className="episode-list-table">
      <thead>
        <tr>
          <th className="episode-list-table-th">Title</th>
          <th className="episode-list-table-th">Date</th>
          <th className="episode-list-table-th">Duration</th>
        </tr>
      </thead>
      <tbody>
        {episodes &&
          episodes.map((episode: EpisodeEntry) => {
            return (
              <TableRow
                key={episode.trackId}
                episode={episode}
                podcastId={podcastId}
              />
            );
          })}
      </tbody>
    </table>
  );
};

export default Table;
