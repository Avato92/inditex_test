import { EpisodeEntry } from "../../../core/domain/episode";

export interface ITableProps {
  episodes: EpisodeEntry[];
  podcastId?: string;
}
