import { useNavigate } from "react-router-dom";
import { formatDate, formatTime } from "../../utils";
import { EpisodeEntry } from "../../../core/domain/episode";
import { useDispatch } from "react-redux";
import { selectEpisode } from "../../../infraestructure/store/episodeSlice";

interface ITableRowProps {
  episode: EpisodeEntry;
  podcastId?: string;
}

const TableRow = ({ episode, podcastId }: ITableRowProps) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const handleClick = (episodeId: number) => {
    dispatch(selectEpisode(episode));
    navigate(`/podcast/${podcastId}/episode/${episodeId}`);
  };

  return (
    <tr className="episode-list-table-tr" key={episode.trackId}>
      <td
        className="episode-list-table-row"
        onClick={() => handleClick(episode.trackId)}
      >
        <span className="episode-list-table-link">{episode.trackName}</span>
      </td>
      <td className="episode-list-table-row">
        {formatDate(new Date(episode.releaseDate))}
      </td>
      <td className="episode-list-table-row">
        {formatTime(episode.trackTimeMillis)}
      </td>
    </tr>
  );
};
export default TableRow;
