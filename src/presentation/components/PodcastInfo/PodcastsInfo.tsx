import { PodcastDetailsInfo } from "../../../core/domain/episode";

interface IPodcastProps {
  podcastInfo: PodcastDetailsInfo;
  description?: string;
}

const PodcastsInfo = ({ podcastInfo, description }: IPodcastProps) => {
  return (
    <div className="episode-podcast-card">
      <img
        className="podcast-card-image"
        srcSet={`${podcastInfo.artworkUrl30} 30w, 
          ${podcastInfo.artworkUrl60} 60w, 
          ${podcastInfo.artworkUrl100} 100w, 
          ${podcastInfo.artworkUrl600} 600w,`}
        src={podcastInfo?.artworkUrl600}
        alt={podcastInfo?.collectionName}
      />
      <div className="podcast-card-text">
        <span className="podcast-card-text-bold">
          {podcastInfo?.collectionName}
        </span>
        <span>{podcastInfo?.artistName}</span>
      </div>
      <div className="podcast-card-description-container">
        <h4>Description:</h4>
        <span>{description}</span>
      </div>
    </div>
  );
};
export default PodcastsInfo;
