import { render, screen } from "@testing-library/react";
import Badge from "./Badge";
import "@testing-library/jest-dom";

describe("Badge", () => {
  it("renders the badge with the correct value", () => {
    const value = "5";
    render(<Badge value={value} />);
    const badge = screen.getByRole("badge");
    expect(badge).toHaveTextContent(value);
  });

  it("renders the badge with a number value as a string", () => {
    const value = 5;
    render(<Badge value={value} />);
    const badge = screen.getByRole("badge");
    expect(badge).toHaveTextContent(value.toString());
  });

  it("renders the badge with a string value", () => {
    const value = "new";
    render(<Badge value={value} />);
    const badge = screen.getByRole("badge");
    expect(badge).toHaveTextContent(value);
  });

  it("renders the badge with the correct className", () => {
    const value = "5";
    render(<Badge value={value} />);
    const badge = screen.getByRole("badge");
    expect(badge).toHaveClass("badge-blue");
  });
});
