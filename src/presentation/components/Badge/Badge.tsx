import { IBadge } from "./types";
import "./badge.css";

const Badge = ({ value }: IBadge) => {
  return (
    <label role="badge" className="badge-blue">
      {value}
    </label>
  );
};
export default Badge;
