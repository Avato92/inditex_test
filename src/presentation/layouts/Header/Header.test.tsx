import { render } from "@testing-library/react";
import "@testing-library/jest-dom";
import Header from "./Header";
import { MemoryRouter } from "react-router-dom";
import { Provider } from "react-redux";
import { store } from "../../../infraestructure/store/store";

jest.mock("../../hooks/useLoadingIndicator", () => {
  return () => false;
});

test("renders header without loading indicator", () => {
  const { getByText, queryByTestId } = render(
    <Provider store={store}>
      <MemoryRouter>
        <Header />
      </MemoryRouter>
    </Provider>
  );
  const headerText = getByText(/Podcaster/i);
  expect(headerText).toBeInTheDocument();

  const loadingIndicator = queryByTestId("loading-indicator ");
  expect(loadingIndicator).not.toBeInTheDocument();
});

jest.mock("../../hooks/useLoadingIndicator", () => {
  return () => true;
});

test("renders header with loading indicator", () => {
  const { getByText, getByTestId } = render(
    <Provider store={store}>
      <MemoryRouter>
        <Header />
      </MemoryRouter>
    </Provider>
  );
  const headerText = getByText(/Podcaster/i);
  expect(headerText).toBeInTheDocument();
  const loadingIndicator = getByTestId("loading-indicator");
  expect(loadingIndicator).toBeInTheDocument();
});
