import "./header.css";
import useLoadingIndicator from "../../hooks/useLoadingIndicator";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { resetPodcastSelected } from "../../../infraestructure/store/podcastSlice";
import { resetEpisodeSelected } from "../../../infraestructure/store/episodeSlice";

const Header = () => {
  const navigate = useNavigate();
  const isLoading = useLoadingIndicator();
  const dispatch = useDispatch();

  const handleClick = () => {
    dispatch(resetPodcastSelected());
    dispatch(resetEpisodeSelected());
    navigate("/");
  };

  return (
    <header className="header">
      <span className="header-text" onClick={handleClick}>
        Podcaster
      </span>
      {isLoading && <span data-testid="loading-indicator" className="loader" />}
    </header>
  );
};

export default Header;
