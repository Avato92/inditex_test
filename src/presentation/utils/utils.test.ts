import { formatTime, formatDate } from "./index";

describe("formatTime", () => {
  it("returns an empty string when duration is NaN", () => {
    expect(formatTime(NaN)).toBe("");
  });

  it("returns a time string in the format mm:ss when duration is less than an hour", () => {
    expect(formatTime(30000)).toBe("0:30");
    expect(formatTime(60000)).toBe("1:00");
    expect(formatTime(61000)).toBe("1:01");
    expect(formatTime(3599000)).toBe("59:59");
  });

  it("returns a time string in the format hh:mm:ss when duration is greater than or equal to an hour", () => {
    expect(formatTime(3600000)).toBe("1:00:00");
    expect(formatTime(3661000)).toBe("1:01:01");
    expect(formatTime(7201000)).toBe("2:00:01");
  });
});

describe("formatDate", () => {
  it("returns a date string in the format dd/mm/yyyy", () => {
    expect(formatDate(new Date("2022-01-01"))).toBe("01/01/2022");
    expect(formatDate(new Date("2022-10-10"))).toBe("10/10/2022");
    expect(formatDate(new Date("2022-12-31"))).toBe("31/12/2022");
  });
});
