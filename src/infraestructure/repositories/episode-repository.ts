import { EpisodeListOutput } from "../../core/domain/episode";
import { EpisodeRepository } from "../../core/ports/episode-repository";
import { getWithExpiry, setWithExpiry } from "../services/localStorage";
import {
  EPISODES_LOCALSTORAGE_KEY,
  DAY_IN_MILISECONDS,
} from "../services/localStorage.constants";

export class RestEpisodeRepository implements EpisodeRepository {
  private readonly baseUrl: string;

  constructor(baseUrl: string) {
    this.baseUrl = baseUrl;
  }

  async list(podcastId: string): Promise<EpisodeListOutput> {
    const storedEpisodes = getWithExpiry(EPISODES_LOCALSTORAGE_KEY + podcastId);
    if (storedEpisodes) {
      const [podcastInfo, ...episodes] = storedEpisodes;
      return { podcastInfo, episodeEntry: episodes };
    } else {
      const response = await fetch(
        `https://api.allorigins.win/get?url=${encodeURIComponent(
          `${this.baseUrl}${podcastId}&media=podcast&entity=podcastEpisode&limit=20`
        )}`
      );
      const data = await response.json();
      const [podcastInfo, ...episodes] = JSON.parse(data.contents).results;
      setWithExpiry(
        EPISODES_LOCALSTORAGE_KEY + podcastId,
        JSON.parse(data.contents).results,
        DAY_IN_MILISECONDS
      );
      return { podcastInfo, episodeEntry: episodes };
    }
  }
}
