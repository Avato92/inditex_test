import { PodcastEntry } from "../../core/domain/podcast";
import { PodcastRepository } from "../../core/ports/podcast-repository";
import { getWithExpiry, setWithExpiry } from "../services/localStorage";
import {
  PODCAST_LOCALSTORAGE_KEY,
  DAY_IN_MILISECONDS,
} from "../services/localStorage.constants";

export class RestPodcastRepository implements PodcastRepository {
  private readonly baseUrl: string;

  constructor(baseUrl: string) {
    this.baseUrl = baseUrl;
  }

  async list(): Promise<PodcastEntry[]> {
    const storedPodcasts = getWithExpiry(PODCAST_LOCALSTORAGE_KEY);
    if (storedPodcasts) {
      return storedPodcasts;
    } else {
      const response = await fetch(this.baseUrl);
      const data = await response.json();
      setWithExpiry(
        PODCAST_LOCALSTORAGE_KEY,
        data.feed.entry,
        DAY_IN_MILISECONDS
      );
      return data.feed.entry;
    }
  }
}
