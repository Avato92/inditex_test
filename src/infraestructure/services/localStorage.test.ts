import { setWithExpiry, getWithExpiry } from "./localStorage";

describe("localStorageUtils", () => {
  beforeEach(() => {
    localStorage.clear();
  });

  describe("setWithExpiry", () => {
    it("should set an item in localStorage with a specified expiry time", () => {
      const key = "testKey";
      const value = "testValue";
      const ttl = 50000;

      setWithExpiry(key, value, ttl);

      const storedItem = getWithExpiry(key);
      expect(storedItem).toBe(value);
    });
  });

  describe("getWithExpiry", () => {
    it("should return null for non-existent keys", () => {
      const result = getWithExpiry("nonExistentKey");
      expect(result).toBeNull();
    });

    it("should return null for expired items", () => {
      const key = "testKey";
      const value = "testValue";
      const ttl = -5000;

      setWithExpiry(key, value, ttl);

      const result = getWithExpiry(key);
      expect(result).toBeNull();
    });

    it("should return the value of a valid item", () => {
      const key = "testKey";
      const value = "testValue";
      const ttl = 5000;

      setWithExpiry(key, value, ttl);

      const result = getWithExpiry(key);
      expect(result).toBe(value);
    });
  });
});
