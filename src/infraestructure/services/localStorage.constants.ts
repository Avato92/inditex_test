export const PODCAST_LOCALSTORAGE_KEY = "podcasts";
export const EPISODES_LOCALSTORAGE_KEY = "episodes";
export const DAY_IN_MILISECONDS = 864000000;
