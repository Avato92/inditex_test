import { configureStore } from "@reduxjs/toolkit";
import podcastReducer from "./podcastSlice";
import episodeReducer from "./episodeSlice";

export const store = configureStore({
  reducer: {
    podcast: podcastReducer,
    episode: episodeReducer,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
