import { createSlice } from "@reduxjs/toolkit";
import type { PayloadAction } from "@reduxjs/toolkit";
import { EpisodeEntry } from "../../core/domain/episode";

export interface EpisodeState {
  episodeSelected: EpisodeEntry | null;
}

const initialState: EpisodeState = {
  episodeSelected: null,
};

export const episodeSlice = createSlice({
  name: "episode",
  initialState,
  reducers: {
    selectEpisode: (state, action: PayloadAction<EpisodeEntry>) => {
      state.episodeSelected = action.payload;
    },
    resetEpisodeSelected: (state) => {
      state.episodeSelected = null;
    },
  },
});

export const { selectEpisode, resetEpisodeSelected } = episodeSlice.actions;

export default episodeSlice.reducer;
