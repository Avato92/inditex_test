import { createSlice } from "@reduxjs/toolkit";
import type { PayloadAction } from "@reduxjs/toolkit";
import { PodcastEntry } from "../../core/domain/podcast";

export interface PodcastState {
  podcastSelected: PodcastEntry | null;
}

const initialState: PodcastState = {
  podcastSelected: null,
};

export const podcastSlice = createSlice({
  name: "podcast",
  initialState,
  reducers: {
    selectPodcast: (state, action: PayloadAction<PodcastEntry>) => {
      state.podcastSelected = action.payload;
    },
    resetPodcastSelected: (state) => {
      state.podcastSelected = null;
    },
  },
});

export const { selectPodcast, resetPodcastSelected } = podcastSlice.actions;

export default podcastSlice.reducer;
