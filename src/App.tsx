import AppRouter from "./presentation/routers/AppRouter";
import { store } from "./infraestructure/store/store";
import { Provider } from "react-redux";

function App() {
  return (
    <Provider store={store}>
      <AppRouter />
    </Provider>
  );
}

export default App;
