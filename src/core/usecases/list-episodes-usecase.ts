import { EpisodeListOutput } from "./../domain/episode";
import { EpisodeRepository } from "./../ports/episode-repository";

export class ListEpisodesUseCase {
  private readonly episodeRepository: EpisodeRepository;

  constructor(episodeRepository: EpisodeRepository) {
    this.episodeRepository = episodeRepository;
  }

  async execute(podcastId: string): Promise<EpisodeListOutput> {
    return this.episodeRepository.list(podcastId);
  }
}
