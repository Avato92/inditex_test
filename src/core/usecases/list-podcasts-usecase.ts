import { PodcastRepository } from "../ports/podcast-repository";
import { PodcastEntry } from "../domain/podcast";

export class ListPodcastsUseCase {
  private readonly podcastRepository: PodcastRepository;

  constructor(podcastRepository: PodcastRepository) {
    this.podcastRepository = podcastRepository;
  }

  async execute(): Promise<PodcastEntry[]> {
    return this.podcastRepository.list();
  }
}
