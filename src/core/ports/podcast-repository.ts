import { PodcastEntry } from "../domain/podcast";

export interface PodcastRepository {
  list(): Promise<PodcastEntry[]>;
}
