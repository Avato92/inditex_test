import { EpisodeListOutput } from "../domain/episode";

export interface EpisodeRepository {
  list(podcastId: string): Promise<EpisodeListOutput>;
}
