interface IGenre {
  name: string;
  id: string;
}

interface Episode {
  kind: "podcast" | "podcast-episode";
  wrapperType: string;
  collectionId: number;
  trackId: number;
  collectionName: string;
  trackName: string;
  artistViewUrl: string;
  collectionViewUrl: string;
  feedUrl: string;
  trackViewUrl: string;
  artworkUrl60: string;
  releaseDate: Date;
  trackTimeMillis: number;
  country: string;
  artworkUrl600: string;
}

export type PodcastDetailsInfo = Episode & {
  kind: "podcast";
  artistId: number;
  artistName: string;
  collectionCensoredName: string;
  trackCensoredName: string;
  artworkUrl30: string;
  artworkUrl100: string;
  collectionPrice: number;
  trackPrice: number;
  collectionHdPrice: number;
  collectionExplicitness: string;
  trackExplicitness: string;
  trackCount: number;
  currency: string;
  primaryGenreName: string;
  genreIds: number[];
  genres: string[];
};

export type EpisodeEntry = Episode & {
  kind: "podcast-episode";
  artistIds: number[];
  closedCaptioning: string;
  description: string;
  shortDescription: string;
  episodeUrl: string;
  episodeFileExtension: string;
  episodeContentType: string;
  artworkUrl160: string;
  previewUrl: string;
  episodeGuid: string;
  genres: IGenre[];
};

export interface EpisodeListOutput {
  podcastInfo: PodcastDetailsInfo;
  episodeEntry: EpisodeEntry[];
}
